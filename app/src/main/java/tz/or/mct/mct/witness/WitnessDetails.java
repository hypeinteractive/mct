package tz.or.mct.mct.witness;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import tz.or.mct.mct.MainActivity;
import tz.or.mct.mct.R;
import tz.or.mct.mct.api.ParamKeys;
import tz.or.mct.mct.http.Request;

public class WitnessDetails extends AppCompatActivity implements View.OnClickListener, Request.OnResponseReady {
    List<String> checkedNews = new ArrayList<>();
    private EditText nameViolotor, mediaOutletName, publisher, titleVialotor, victimName, etWitnessName;
    private CheckBox checkBoxBanning, checkBoxArrest, checkBoxBattery, checkBoxKidnap, checkBoxThreats, checkBoxMurder, checkBoxViolet, checkBoxAssault;
    private Button sendButton;
    private PreferenceTracking pref;
    private EditText cuprit_title, cuprit_name;
    private CheckBox checkBoxDamage;
    private CheckBox checkBoxVConsification;
    private CheckBox checkBoxCyberAttack;
    private RadioGroup culpritGroup;
    private RadioButton rb_state;
    private RadioGroup newsTypetGroup;
    private RadioButton rbMediaOwners, rbMobAssault, rbAdvertisers, rbPolitician;
    private RadioButton rbNewsPolitics, rbNewsSocial, rbNewsGoverment, rbNewSports, rbNewsEconomic, rbNewsEnvironment;
    private RadioGroup severeintyGroup;
    private RadioButton rbCritical, rbHigh, rbLow, rbMedium;
    private RadioGroup mediatypeGroup;
    private RadioButton rbPrint, rbTv, rbRadio, rbOnline;
    private CheckBox checkBoxHarrasment;
    private String witnesName, victName, sexType, witnessTitle, witnessName, witnessVictimName, witnessPhoneNumber, witnessIncident, witnessIncidentDate, witnessGender, witnessLatitude, longitude, latitude;
    private String cupritName;
    private String cupritTitle;
    private RadioButton radioSelectedCuprtit;
    private String selectedcupritType;
    private RadioButton radioSelectedNewsType;
    private String selectedNews;
    private RadioButton radioSelectedSevereint;
    private String selectedSevereints;
    private RadioButton radioSelecteTypeOfMedia;
    private String selectedsMediaType;
    private ProgressDialog pDialog;
    private RadioGroup sexGroup;
    private RadioButton radioSex;
    private CheckBox checkBoxDenied;
    final Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_witness_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initViews();
        ActionBar actionBar = getSupportActionBar();
        actionBar.setLogo(R.mipmap.ic_logo);
        actionBar.setDisplayUseLogoEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        sendButton.setOnClickListener(this);
        checkBoxBattery.setOnClickListener(this);
        checkBoxAssault.setOnClickListener(this);
        checkBoxBanning.setOnClickListener(this);
        checkBoxThreats.setOnClickListener(this);
        checkBoxKidnap.setOnClickListener(this);
        checkBoxViolet.setOnClickListener(this);
        checkBoxMurder.setOnClickListener(this);
        checkBoxArrest.setOnClickListener(this);
        checkBoxDenied.setOnClickListener(this);
        checkBoxHarrasment.setOnClickListener(this);
        checkBoxDamage.setOnClickListener(this);
        checkBoxCyberAttack.setOnClickListener(this);
        checkBoxVConsification.setOnClickListener(this);
        getingSharedPreference();
    }

    @Nullable
    public Intent getParentActivityIntent() {
        return super.getParentActivityIntent();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() != android.R.id.home) {
            return super.onOptionsItemSelected(item);
        }
        finish();
        return true;
    }

    public Intent getSupportParentActivityIntent() {
        finish();
        return new Intent();
    }

    public void onCreateSupportNavigateUpTaskStack(TaskStackBuilder builder) {
        super.onCreateSupportNavigateUpTaskStack(builder);
    }

    public void initViews() {
        cuprit_name = (EditText) findViewById(R.id.cuprit_name);
        cuprit_title = (EditText) findViewById(R.id.cuprit_title);
        checkBoxArrest = (CheckBox) findViewById(R.id.cb_arrest);
        checkBoxBanning = (CheckBox) findViewById(R.id.cb_banning);
        checkBoxBattery = (CheckBox) findViewById(R.id.cb_battery);
        checkBoxKidnap = (CheckBox) findViewById(R.id.cb_kidnap);
        checkBoxThreats = (CheckBox) findViewById(R.id.cb_threats);
        checkBoxMurder = (CheckBox) findViewById(R.id.cb_murder);
        checkBoxAssault = (CheckBox) findViewById(R.id.cb_assault);
        checkBoxViolet = (CheckBox) findViewById(R.id.cb_violet);
        checkBoxHarrasment = (CheckBox) findViewById(R.id.cb_harrasment);
        checkBoxDamage = (CheckBox) findViewById(R.id.cb_damage);
        checkBoxVConsification = (CheckBox) findViewById(R.id.cb_consification);
        checkBoxCyberAttack = (CheckBox) findViewById(R.id.cb_cyber_attack);
        checkBoxDenied = (CheckBox) findViewById(R.id.denied);
        culpritGroup = (RadioGroup) findViewById(R.id.culprit_group);
        rb_state = (RadioButton) findViewById(R.id.rb_state);
        rbMediaOwners = (RadioButton) findViewById(R.id.rb_media_owners);
        rbAdvertisers = (RadioButton) findViewById(R.id.rb_advertisers);
        rbPolitician = (RadioButton) findViewById(R.id.rb_politician);
        rbMobAssault = (RadioButton) findViewById(R.id.rb_mob_assaut);

        newsTypetGroup = (RadioGroup) findViewById(R.id.newstype_group);
        rbNewsPolitics = (RadioButton) findViewById(R.id.rb_Politics);
        rbNewsSocial = (RadioButton) findViewById(R.id.rb_social);
        rbNewSports = (RadioButton) findViewById(R.id.rb_sports);
        rbNewsEnvironment = (RadioButton) findViewById(R.id.rb_environmvent);
        rbNewsGoverment = (RadioButton) findViewById(R.id.rb_goverment);
        rbNewsEconomic = (RadioButton) findViewById(R.id.rb_Economics);


        severeintyGroup = (RadioGroup) findViewById(R.id.serereinty_group);
        rbCritical = (RadioButton) findViewById(R.id.rb_Critical);
        rbHigh = (RadioButton) findViewById(R.id.rb_high);
        rbLow = (RadioButton) findViewById(R.id.rb_low);
        rbMedium = (RadioButton) findViewById(R.id.rb_medium);

        mediatypeGroup = (RadioGroup) findViewById(R.id.mediatype_group);
        rbPrint = (RadioButton) findViewById(R.id.rb_print);
        rbRadio = (RadioButton) findViewById(R.id.rb_radio);
        rbOnline = (RadioButton) findViewById(R.id.rb_online);
        rbTv = (RadioButton) findViewById(R.id.rb_tv);


        sendButton = (Button) findViewById(R.id.witness_details_submit);
        victimName = (EditText) findViewById(R.id.name_victim);
        etWitnessName = (EditText) findViewById(R.id.witness_name);
        sexGroup = (RadioGroup) findViewById(R.id.rg_sex);

    }

    void getingSharedPreference() {
        pref = new PreferenceTracking(getApplicationContext());
        witnessTitle = pref.getWitnessTitle();
        witnessIncident = pref.getWitnessIncident();
        witnessIncidentDate = pref.getWitnessIncidentDate();
        latitude = pref.getWitLatitude();
        longitude = pref.getWitLongitude();
        witnessVictimName = pref.getWitVictimName();
        witnessGender = pref.getWitnessSex();
        witnessName = pref.getWitnessName();
        witnessPhoneNumber = pref.getMobileNumber();

    }

    public void getTextField() {
        getingSharedPreference();
        witnesName = etWitnessName.getText().toString().trim();
        victName = victimName.getText().toString().trim();
        cupritName = cuprit_name.getText().toString().trim();
        cupritTitle = cuprit_title.getText().toString().trim();
        //CHECKING VALUE FOR GENDER
        int selectedId = sexGroup.getCheckedRadioButtonId();
        radioSex = (RadioButton) findViewById(selectedId);
        String sexSend = radioSex == null ? "" : radioSex.getText().toString();
        if (sexSend.equalsIgnoreCase("MME") || sexSend.equalsIgnoreCase("Male")) {
            sexSend = "Male";
        } else if (sexSend.equalsIgnoreCase("MKE") || sexSend.equalsIgnoreCase("Female")) {
            sexSend = "Female";
        }
        sexType = radioSex != null ? sexSend : "";

        //CHECKING FOR CULPRIT TYPE
        int selectedCuptrit = culpritGroup.getCheckedRadioButtonId();
        radioSelectedCuprtit = (RadioButton) findViewById(selectedCuptrit);
        String sendCulprit = radioSelectedCuprtit == null ? "" : radioSelectedCuprtit.getText().toString();
        if (sendCulprit.equalsIgnoreCase("State Coercive Organs (intelligence and security agents, police, local government, militia)") || sendCulprit.equalsIgnoreCase("Mamlaka za nchi (mawakala wa usalama na upelelezi, polisi, serikali ya mtaa, jeshi)")) {
            sendCulprit = "State Coercive Organs (intelligence and security agents, police, local government, militia)";
        } else if (sendCulprit.equalsIgnoreCase("Media owners/Shareholders/Directors") || sendCulprit.equalsIgnoreCase("Wamiliki/Waongozaji/Washiriki wa vyombo ya habari")) {
            sendCulprit = "Media owners/Shareholders/Directors";
        } else if (sendCulprit.equalsIgnoreCase("Advertisers, Business and Political Allies of Owners") || sendCulprit.equalsIgnoreCase("Watangazaji, Biashara na Washirika wa kisiasa wa wamiliki")) {
            sendCulprit = "Advertisers, Business and Political Allies of Owners";
        } else if (sendCulprit.equalsIgnoreCase("Politicians/State Functionaries") || sendCulprit.equalsIgnoreCase("Wanasiasa/Watendaji wa nchi")) {
            sendCulprit = "Politicians/State Functionaries";
        } else if (sendCulprit.equalsIgnoreCase("Mob Assault") || sendCulprit.equalsIgnoreCase("Shambulizi la watu wengi")) {
            sendCulprit = "Mob Assault";
        }
        selectedcupritType = radioSelectedCuprtit != null ? sendCulprit : "";

        //CHECKING FOR TYPE OF NEWS
        int selectedNewsType = newsTypetGroup.getCheckedRadioButtonId();
        radioSelectedNewsType = (RadioButton) findViewById(selectedNewsType);
        String TypeOfNewsSend = radioSelectedNewsType == null ? "" : radioSelectedNewsType.getText().toString();
        if (TypeOfNewsSend.equalsIgnoreCase("Politics") || TypeOfNewsSend.equalsIgnoreCase("Siasa")) {
            TypeOfNewsSend = "Politics";
        } else if (TypeOfNewsSend.equalsIgnoreCase("Social") || TypeOfNewsSend.equalsIgnoreCase("Jamii")) {
            TypeOfNewsSend = "Social";
        } else if (TypeOfNewsSend.equalsIgnoreCase("Economics/Business") || TypeOfNewsSend.equalsIgnoreCase("Uchumi/Biashara")) {
            TypeOfNewsSend = "Economics/Business";
        } else if (TypeOfNewsSend.equalsIgnoreCase("Environment") || TypeOfNewsSend.equalsIgnoreCase("Mazingira")) {
            TypeOfNewsSend = "Environment";
        } else if (TypeOfNewsSend.equalsIgnoreCase("Government") || TypeOfNewsSend.equalsIgnoreCase("Serikali")) {
            TypeOfNewsSend = "Government";
        } else if (TypeOfNewsSend.equalsIgnoreCase("Sports") || TypeOfNewsSend.equalsIgnoreCase("Michezo")) {
            TypeOfNewsSend = "Sports";
        }
        selectedNews = radioSelectedNewsType != null ? TypeOfNewsSend : "";

        //CHECKING FOR INCIDENT SEVERITY
        int selectedSevereinty = severeintyGroup.getCheckedRadioButtonId();
        radioSelectedSevereint = (RadioButton) findViewById(selectedSevereinty);
        String severitySend = radioSelectedSevereint == null ? "" : radioSelectedSevereint.getText().toString();
        if (severitySend.equalsIgnoreCase("Critical") || severitySend.equalsIgnoreCase("Kubwa sana")) {
            severitySend = "Critical";
        } else if (severitySend.equalsIgnoreCase("High") || severitySend.equalsIgnoreCase("Kubwa")) {
            severitySend = "High";
        } else if (severitySend.equalsIgnoreCase("Medium") || severitySend.equalsIgnoreCase("Kati kati")) {
            severitySend = "Medium";
        } else if (severitySend.equalsIgnoreCase("Low") || severitySend.equalsIgnoreCase("Dogo")) {
            severitySend = "Low";
        }
        selectedSevereints = radioSelectedSevereint != null ? severitySend : "";
        //CHECKING FOR MEDIA TYPE
        int selectedMediaType = mediatypeGroup.getCheckedRadioButtonId();
        radioSelecteTypeOfMedia = (RadioButton) findViewById(selectedMediaType);
        String TypeOfMediaSend = radioSelecteTypeOfMedia == null ? "" : radioSelecteTypeOfMedia.getText().toString();
        if (TypeOfNewsSend.equalsIgnoreCase("Print") || TypeOfMediaSend.equalsIgnoreCase("Chapisho")) {
            TypeOfMediaSend = "Print";
        } else if (TypeOfMediaSend.equalsIgnoreCase("Radio") || TypeOfMediaSend.equalsIgnoreCase("Redio")) {
            TypeOfMediaSend = "Radio";
        } else if (TypeOfMediaSend.equalsIgnoreCase("TV") || TypeOfMediaSend.equalsIgnoreCase("TV")) {
            TypeOfMediaSend = "TV";
        } else if (TypeOfMediaSend.equalsIgnoreCase("Online Media") || TypeOfMediaSend.equalsIgnoreCase("Chombo cha mtandaoni")) {
            TypeOfMediaSend = "Online Media";
        }
        selectedsMediaType = radioSelecteTypeOfMedia != null ? TypeOfMediaSend : "";

    }

    @Override
    public void onClick(View view) {
        if (view == sendButton) {
            getTextField();
            sendParams();
        } else if (view == checkBoxDenied) {
            if (checkBoxDenied.isChecked()) {
                checkedNews.add("1");

            } else {
                checkedNews.remove("1");
            }
        } else if (view == checkBoxMurder) {
            if (checkBoxMurder.isChecked()) {
                checkedNews.add("3");

            } else {
                checkedNews.remove("3");
            }

        } else if (view == checkBoxBattery) {
            if (checkBoxBattery.isChecked()) {
                checkedNews.add("8");

            } else {
                checkedNews.remove("8");
            }

        } else if (view == checkBoxArrest) {
            if (checkBoxArrest.isChecked()) {
                checkedNews.add("5");

            } else {
                checkedNews.remove("5");
            }

        } else if (view == checkBoxThreats) {
            if (checkBoxThreats.isChecked()) {
                checkedNews.add("2");

            } else {
                checkedNews.remove("2");
            }
        } else if (view == checkBoxKidnap) {
            if (checkBoxKidnap.isChecked()) {
                checkedNews.add("6");

            } else {
                checkedNews.remove("6");
            }

        } else if (view == checkBoxAssault) {
            if (checkBoxAssault.isChecked()) {
                checkedNews.add("7");

            } else {
                checkedNews.remove("7");
            }

        } else if (view == checkBoxBanning) {
            if (checkBoxBanning.isChecked()) {
                checkedNews.add("9");

            } else {
                checkedNews.remove("9");
            }

        } else if (view == checkBoxCyberAttack) {
            if (checkBoxCyberAttack.isChecked()) {
                checkedNews.add("13");

            } else {
                checkedNews.remove("13");
            }

        } else if (view == checkBoxVConsification) {
            if (checkBoxVConsification.isChecked()) {
                checkedNews.add("11");

            } else {
                checkedNews.remove("11");
            }

        } else if (view == checkBoxViolet) {
            if (checkBoxViolet.isChecked()) {
                checkedNews.add("10");

            } else {
                checkedNews.remove("10");
            }

        } else if (view == checkBoxDamage) {
            if (checkBoxDamage.isChecked()) {
                checkedNews.add("12");

            } else {
                checkedNews.remove("12");
            }

        } else if (view == checkBoxHarrasment) {
            if (checkBoxHarrasment.isChecked()) {
                checkedNews.add("4");
            } else {
                checkedNews.remove("4");
            }
        }
    }


    void sendParams() {
        pDialog = new ProgressDialog(WitnessDetails.this);
        pDialog.setTitle(R.string.loading);
        pDialog.setMessage(getString(R.string.please));
        pDialog.setIndeterminate(true);
        pDialog.setCancelable(false);
        pDialog.show();
        HashMap<String, String> params = getParams();
        new tz.or.mct.mct.http.Request(this, tz.or.mct.mct.http.Request.RequestType.WITNESS_REPORT, params)
                .setOnResponseReadyListener(this)
                .setMethod(Request.METHOD_POST)
                .send();

    }

    String sendCheckedNews() {
        JSONArray checkedArray = new JSONArray();
        for (int i = 0; i < checkedNews.size(); i++) {
            checkedArray.put(checkedNews.get(i));
        }
        return checkedArray.toString();

    }

    public HashMap<String, String> getParams() {
        String checkedIncidents = sendCheckedNews();
        HashMap<String, String> params = new HashMap<>();

        if (!witnesName.equals("")) {
            params.put(ParamKeys.WITNESS_NAME, witnesName);
            pref.setWitnessName(witnesName);
        }
        if (!victName.equals("")) {
            params.put(ParamKeys.WITNESS_VICTIMS_NAME, victName);
            pref.setWitnessVictimName(victName);
        }
        if (!sexType.equals("")) {
            params.put(ParamKeys.WITNESS_GENDER, sexType);
            pref.setWitnessSex(sexType);
        }
        if (!cuprit_name.equals("")) {
            params.put(ParamKeys.WITNESS_CULPRIT_NAME, cupritName);
        }

        if (!cuprit_title.equals("")) {
            params.put(ParamKeys.WITNESS_CULPRIT_TITLE, cupritTitle);
        }

        if (!witnessGender.equals("")) {
            params.put(ParamKeys.WITNESS_GENDER, witnessGender);

        }
        if (!witnessTitle.equals("")) {
            params.put(ParamKeys.TITLE, witnessTitle);
        }
        if (!witnessIncident.equals("")) {
            params.put(ParamKeys.WITNESS_INCIDENT_DATE, witnessIncidentDate);
        }
        if (!witnessPhoneNumber.equals("")) {
            params.put(ParamKeys.WITNESS_PHONE_NUMBER, witnessPhoneNumber);

        }
        if (!String.valueOf(latitude).equals("")) {
            params.put(ParamKeys.LOCATION_LAT, String.valueOf(latitude));
        }
        if (!String.valueOf(longitude).equals("")) {
            params.put(ParamKeys.LOCATION_LONG, String.valueOf(longitude));

        }
        if (!witnessName.equals("")) {
            params.put(ParamKeys.WITNESS_NAME, witnessName);

        }

        if (!witnessVictimName.equals("")) {
            params.put(ParamKeys.WITNESS_VICTIMS_NAME, witnessVictimName);

        }


        if (!selectedcupritType.equals("")) {
            params.put(ParamKeys.WITNESS_CULPRIT_TYPE, selectedcupritType);

        }
        if (!selectedNews.equals("")) {
            params.put(ParamKeys.WITNESS_NEWS_TYPE, selectedNews);

        }

        if (!selectedSevereints.equals("")) {
            params.put(ParamKeys.WITNESS_INCIDENT_SEVERITY, selectedSevereints);

        }
        if (!checkedIncidents.equals("")) {
            params.put(ParamKeys.WITNESS_NATURE_OF_VIOLATION, checkedIncidents);
        }

        if (!selectedsMediaType.equals("")) {
            params.put(ParamKeys.WITNESS_MEDIA_TYPE, selectedsMediaType);

        }

        return params;
    }

    @Override
    public void onResponseFailed(String msg) {
        pDialog.dismiss();
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();

    }

    @Override
    public void onResponseOk(JSONObject jSONObject) {
        pDialog.dismiss();
        showFinalDialog();
        //showDialogResponse();
    }
    public void showDialogResponse() {
        AlertDialog.Builder builder = new AlertDialog.Builder(WitnessDetails.this);
        builder.setMessage(R.string.submitted);
        builder.setPositiveButton(R.string.OK, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(WitnessDetails.this, MainActivity.class);
                startActivity(intent);
                dialogInterface.dismiss();
            }
        });
        AlertDialog diag = builder.create();
        diag.show();
    }

    private void showFinalDialog(){
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.custom_dialog_final);
        dialog.setTitle(R.string.submitted);
        TextView text = (TextView) dialog.findViewById(R.id.tv_header);
        text.setText(R.string.submitted_final);
        Button bn_ok = (Button) dialog.findViewById(R.id.bn_final);
        bn_ok.setText(R.string.OK);
        bn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(WitnessDetails.this, MainActivity.class);
                startActivity(intent);
                dialog.dismiss();
            }
        });
        dialog.show();
    }

}