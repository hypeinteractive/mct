package tz.or.mct.mct.http;

import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.ProtocolException;
import java.net.SocketTimeoutException;


public class HttpRequestAsyncTask extends AsyncTask<JSONObject, Void, JSONObject> {
    private Context context;
    private final Request.OnResponseReady onResponseReady;
    private final String path;
    private String authHeader;

    public HttpRequestAsyncTask(Context context, String url, Request.OnResponseReady orr) {
        this.context = context;
        this.path = url;
        this.onResponseReady = orr;
    }

    public void setAuthHeader(String authHeader) {
        this.authHeader = authHeader;
    }

    public String getAuthHeader() {
        return authHeader;
    }

    @Override
    protected void onPreExecute(){
        super.onPreExecute();
    }

    @Override
    protected JSONObject doInBackground(JSONObject... params) {
        try {
            HttpConnection hCon = new HttpConnection(this.path, this.context);
            hCon.setAuthHeader(authHeader);
            return  hCon.sendRequest(params[0],Request.METHOD_POST);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        } catch (ProtocolException e2) {
            e2.printStackTrace();
            return null;
        } catch (SocketTimeoutException e3) {
            e3.printStackTrace();
            return null;
        } catch (IOException e4) {
            e4.printStackTrace();
            return null;
        }
    }

    protected void onPostExecute(JSONObject res) {
        super.onPostExecute(res);
        if (res!=null){
            success(res);
        }else{
            failed("Network Error");
        }
    }

    private void success(JSONObject content) {
        if (this.onResponseReady != null) {
            this.onResponseReady.onResponseOk(content);
        }
    }

    private void failed(String s) {
        if (this.onResponseReady != null) {
            this.onResponseReady.onResponseFailed(s);
        }
    }
}
