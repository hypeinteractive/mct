package tz.or.mct.mct.http;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;

class HttpConnection {
	private HttpURLConnection urlConnection;
    private String authHeader;

    HttpConnection(String address, Context context) throws IOException {
		URL url = new URL(address);
		if(context==null) throw new IOException();
		urlConnection = (HttpURLConnection) url.openConnection();
		urlConnection.setReadTimeout(45000000/*milliseconds*/);
		urlConnection.setConnectTimeout(12000000 /*milliseconds*/);
		urlConnection.setDoOutput(true);
		urlConnection.setRequestProperty("Accept", "*/*");
	}
	
	JSONObject sendRequest(JSONObject paramsObj,String method) throws UnsupportedEncodingException, ProtocolException, SocketTimeoutException{
		urlConnection.setRequestMethod(method);
        urlConnection.setRequestProperty("Authorization",authHeader);
		JSONObject response=null;
		BufferedReader reader=null;
		try{
			urlConnection.connect();
			// Send POST/GET data request
			OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
            Log.v("api data to send:",paramsObj.toString());
            wr.write(paramsObj.toString());
            wr.flush();
            // Get the server response
			reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
			StringBuilder sb = new StringBuilder();
			String line;
			// Read Server Response
			while((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
            if (sb.length()==0 && urlConnection.getResponseCode()==204){
                sb.append("{'response':'ok'}"); //As server returns empty response on success
            }
			System.out.println(sb.toString());
			response = new JSONObject(sb.toString());
		} catch(IOException | JSONException ex){
			ex.printStackTrace();
            try {
                Log.v("api response code",""+urlConnection.getResponseCode());
                Log.v("api response msg",urlConnection.getResponseMessage());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }finally{
			try{
				reader.close();
				urlConnection.disconnect();
			} catch(Exception ex) {
                ex.printStackTrace();
            }
		}
		return response;
	}


    void setAuthHeader(String authHeader) {
        this.authHeader = authHeader;
    }
}
