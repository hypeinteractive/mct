package tz.or.mct.mct.victim;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.seatgeek.placesautocomplete.OnPlaceSelectedListener;
import com.seatgeek.placesautocomplete.PlacesAutocompleteTextView;
import com.seatgeek.placesautocomplete.model.Place;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import tz.or.mct.mct.MainActivity;
import tz.or.mct.mct.R;
import tz.or.mct.mct.api.ParamKeys;
import tz.or.mct.mct.http.Request;
import tz.or.mct.mct.witness.WitnessDetails;

public class VictimActivity extends AppCompatActivity implements View.OnClickListener, OnMapReadyCallback, tz.or.mct.mct.http.Request.OnResponseReady {

    public MapFragment mapFragment;
    EditText etTitle, etIncident, etDate, etLocation;
    Button bnSearch, bnClear, bnSubmit;
    EditText etPhone;
    ImageView ivCalendar;
    GoogleMap myGoogleMap;
    List<String> collectedInformation = new ArrayList<>();
    private DatePickerDialog victimDate;
    private String year0, month0, day0;
    private double latitude, longitude;
    private Marker marker;
    private String locality;
    private String victimPhone;
    private String victimTitle;
    private String victimIncident;
    private String victimReportDate;
    private JSONArray checkedData;
    private ProgressDialog pDialog;
    private String victimLocation;
    final Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_victim);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.tv_heading);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent back = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(back);
                    finish();
                }
            });
        }
        if (googleServicesAvailable()) {
            initMap();
        } else {
            Toast.makeText(getApplicationContext(), R.string.play_store_error, Toast.LENGTH_SHORT).show();
        }
        initViews();
        ActionBar actionBar = getSupportActionBar();
        actionBar.setLogo(R.mipmap.ic_logo);
        actionBar.setDisplayUseLogoEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
    }

    public void initViews() {
        etTitle = (EditText) findViewById(R.id.etTitle);
        etIncident = (EditText) findViewById(R.id.etIncident);
        etDate = (EditText) findViewById(R.id.etDate);
        etPhone = (EditText) findViewById(R.id.etPhone);
        etLocation = (EditText) findViewById(R.id.etLocation);
        bnSearch = (Button) findViewById(R.id.bnSearch);
        bnClear = (Button) findViewById(R.id.bnClear);
        bnSubmit = (Button) findViewById(R.id.bnSubmit);
        ivCalendar = (ImageView) findViewById(R.id.ivCalendar);
        ivCalendar.setOnClickListener(this);
        bnClear.setOnClickListener(this);
        bnSearch.setOnClickListener(this);
        bnSubmit.setOnClickListener(this);
        etDate.setOnClickListener(this);
        ((PlacesAutocompleteTextView)etLocation).setOnPlaceSelectedListener(new OnPlaceSelectedListener() {
            @Override
            public void onPlaceSelected(@NonNull Place place) {
                onSearch(etLocation);
            }
        });
        ((PlacesAutocompleteTextView)etLocation).setLocationBiasEnabled(true);
        Location biasLocation = new Location("none");
        biasLocation.setLatitude(-6.178646);
        biasLocation.setLongitude(35.480995);
        ((PlacesAutocompleteTextView)etLocation).setCurrentLocation(biasLocation);
        ((PlacesAutocompleteTextView)etLocation).setRadiusMeters(632060L);
    }

    public void sendVictimInformation() {
        HashMap params = getVictimInformation();
        new Request(this, Request.RequestType.VICTIM_REPORT, params)
                .setOnResponseReadyListener(this)
                .setMethod(tz.or.mct.mct.http.Request.METHOD_POST)
                .send();
        pDialog = new ProgressDialog(this);
        pDialog.setTitle(R.string.loading);
        pDialog.setMessage(getString(R.string.please));
        pDialog.setIndeterminate(true);
        pDialog.setCancelable(false);
        pDialog.show();
    }

    public HashMap<String, String> getVictimInformation() {
        sendCheckedItems();
        getInfo();
        HashMap<String, String> params = new HashMap<>();

        if (!victimTitle.equals("")) {
            params.put(ParamKeys.TITLE, victimTitle);
        }

        if (!victimIncident.equals("")) {
            params.put(ParamKeys.CONTENT, victimIncident);
        }

        if (!victimReportDate.equals("")) {
            params.put(ParamKeys.VICTIM_INCIDENT_DATE, victimReportDate);
        }

        if (!etLocation.equals("")) {
            params.put(ParamKeys.LOCATION_LAT, String.valueOf(latitude));
        }
        if (!etLocation.equals("")) {
            params.put(ParamKeys.LOCATION_LONG, String.valueOf(longitude));
        }
        if (!victimPhone.equals("")) {
            params.put(ParamKeys.VICTIM_PHONE_NUMBER, victimPhone);
        }
        if (!victimPhone.equals("")) {
            params.put(ParamKeys.VICTIM_PHONE_NUMBER, victimPhone);
        }
        if (checkedData!=null && !checkedData.toString().equalsIgnoreCase("")) {
            params.put(ParamKeys.VICTIM_NATURE_OF_VIOLATION, checkedData.toString());
        }
        return params;
    }

    public void getInfo() {
        victimPhone = etPhone.getText().toString();
        victimTitle = etTitle.getText().toString();
        victimIncident = etIncident.getText().toString();
        victimLocation = etLocation.getText().toString();
        victimReportDate = etDate.getText().toString();
    }

    @Override
    public void onClick(View v) {
        if (v == bnSearch) {
            if (etLocation.equals("")) {
                Toast.makeText(getApplicationContext(), R.string.error_keyword, Toast.LENGTH_SHORT).show();
            } else {
                onSearch(v);
            }
        } else if (v == bnClear) {
            clearData();
        } else if (v == ivCalendar || v==etDate) {
            etDate.setFocusable(false);
            getCalendar();
            victimDate.show();
        } else if (v == bnSubmit) {
            getInfo();
            if (etTitle.getText().length() == 0) {
                etTitle.setError(getString(R.string.et_title));
                etTitle.requestFocus();
            }else if (etIncident.getText().length() == 0) {
                etIncident.setError(getString(R.string.et_incident));
                etIncident.requestFocus();
            } else if (etDate.getText().length() <= 6) {
                etDate.setError(getString(R.string.dateRequired));
                etDate.requestFocus();
            } else if (etLocation.getText().length() == 0) {
                etLocation.setError(getString(R.string.location));
                etLocation.requestFocus();
            } else if ((etPhone.getText().toString().length() <= 9)) {
                etPhone.setError(getString(R.string.phone));
                etPhone.requestFocus();
            } else {
                //showSubmissionDialog();
                showCustomDialog();
            }
        }

    }


    public void showSubmissionDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.more_info);
        builder.setPositiveButton(R.string.witness_provide, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(VictimActivity.this, VictimMoreDetailsActivity.class);
                intent.putExtra("basic_params",getVictimInformation());
                victimPreference();
                startActivityForResult(intent,1);
                dialogInterface.dismiss();
            }
        });
        builder.setNegativeButton(R.string.witness_skip, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                sendVictimInformation();
                dialog.dismiss();
            }
        });
        AlertDialog diag = builder.create();
        diag.show();
    }


    public boolean googleServicesAvailable() {
        GoogleApiAvailability api = GoogleApiAvailability.getInstance();
        int isAvailable = api.isGooglePlayServicesAvailable(this);
        if (isAvailable == ConnectionResult.SUCCESS) {
            return true;
        } else if (api.isUserResolvableError(isAvailable)) {
            Dialog dialog = api.getErrorDialog(this, isAvailable, 0);
            dialog.show();
        } else {
            Toast.makeText(getApplicationContext(), R.string.play_connect, Toast.LENGTH_SHORT).show();
        }
        return false;
    }

    public void getCalendar() {
        final SimpleDateFormat simpleCalendar = new SimpleDateFormat("dd-mm-yyy", Locale.US);
        final Calendar calendar = Calendar.getInstance();
        victimDate = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                year0 = String.valueOf(year);
                month0 = String.valueOf(month + 1);
                day0 = String.valueOf(day);
                Log.e("Today", simpleCalendar.format(calendar.getTime()));
                etDate.setText(day0 + "-" + month0 + "-" + year0);
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
    }


    private void initMap() {
        mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.mapFragment);
        mapFragment.getMapAsync(this);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        myGoogleMap = googleMap;
        LatLng tanzania = new LatLng(-6.3690, 34.8888);
        marker=myGoogleMap.addMarker(new MarkerOptions().position(tanzania).title(locality));
        myGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(tanzania, 6));

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        myGoogleMap.setMyLocationEnabled(true);
    }

    public void onSearch(View v) {
        hideSoftKeyboard(v);
        String location = etLocation.getText().toString();
        List<android.location.Address> adressList = null;
        if (location != null || !location.equals("")) {
            Geocoder geocoder = new Geocoder(this);
            try {
                adressList = geocoder.getFromLocationName(location, 1);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if ((adressList != null ? adressList.size() : 0) > 0) {
                android.location.Address address = adressList.get(0);
                locality = address.getLocality();
                LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());
                if (marker != null) {
                    marker.remove();
                }
                marker = myGoogleMap.addMarker(new MarkerOptions()
                        .position(latLng)
                        .title(locality));
                myGoogleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
                latitude = address.getLatitude();
                longitude = address.getLongitude();
            }
        }
    }

    private void hideSoftKeyboard(View v) {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    private void clearData() {
        if (marker != null) {
            etLocation.setText("");
            marker.remove();
            initMap();
        }
    }

    public void showDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(VictimActivity.this);
        builder.setTitle(R.string.error_dilog);
        builder.setMessage(R.string.error_dilog_title);
        builder.setIcon(R.drawable.absent);
        builder.setNegativeButton(R.string.got_it, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void sendCheckedItems() {
        checkedData = new JSONArray();
        for (int i = 0; i < collectedInformation.size(); i++) {
            checkedData.put(collectedInformation.get(i));
        }
    }

    @Override
    public void onResponseFailed(String msg) {
        pDialog.hide();
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onResponseOk(JSONObject jSONObject) {
        pDialog.hide();
       showDialogResponse();
    }

    public void victimPreference(){
        SharedPreferences victimPreference = getSharedPreferences("victimPreference", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = victimPreference.edit();
        editor.putString("victimPhone",victimPhone);
        editor.putString("victimTittle",victimTitle);
        editor.putString("victimIncidence",victimIncident);
        editor.putString("victimDate",victimReportDate);
        editor.putString("victimLocation",victimLocation);
        editor.commit();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if(resultCode == RESULT_OK){
                String Phone = data.getStringExtra("phone");
                String Tittle = data.getStringExtra("title");
                String Incidence = data.getStringExtra("incidence");
                String Date = data.getStringExtra("date");
                String Location = data.getStringExtra("location");

                etPhone.setText(Phone);
                etLocation.setText(Location);
                etDate.setText(Date);
                etTitle.setText(Tittle);
                etIncident.setText(Incidence);
            }
        }
    }
    public void showDialogResponse() {
        AlertDialog.Builder builder = new AlertDialog.Builder(VictimActivity.this);
        builder.setMessage(R.string.submitted);
        builder.setPositiveButton(R.string.OK, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(VictimActivity.this, MainActivity.class);
                startActivity(intent);
                dialogInterface.dismiss();
            }
        });
        AlertDialog diag = builder.create();
        diag.show();
    }

    private void showCustomDialog(){
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.custom_dialog);
        dialog.setTitle(R.string.more_info);
        TextView text = (TextView) dialog.findViewById(R.id.tv_header);
        text.setText(R.string.submitted_message);
        Button bn_ok = (Button) dialog.findViewById(R.id.bn_ok);
        bn_ok.setText(R.string.OK);
        bn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(VictimActivity.this, VictimMoreDetailsActivity.class);
                intent.putExtra("basic_params",getVictimInformation());
                victimPreference();
                startActivityForResult(intent,1);
                dialog.dismiss();
            }
        });
        Button bn_cancel = (Button) dialog.findViewById(R.id.bn_thank);
        bn_cancel.setText(R.string.witness_skip);
        bn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendVictimInformation();
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}
