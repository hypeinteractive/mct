package tz.or.mct.mct.api;

/**
 * Created by memory on 27/11/2016.
 */

public class ParamKeys {
    public static final String TITLE="title";
    public static final String CONTENT="content";
    public static final String LOCATION_LAT="lat";
    public static final String LOCATION_LONG="lon";
    public static final String API_WITNESS_LOCATION="2763ec6f-ebc9-472b-9546-37dea75f823a";
    public static final String WITNESS_GENDER="6021b406-0f3c-4034-a00e-8ba4e27e5d6c";
    public static final String WITNESS_NAME="e98d2ccd-8e45-4422-ad1f-284de43583ae";
    public static final String WITNESS_VICTIMS_NAME="50b15990-df86-44d9-b366-52fca14dca27";
    public static final String WITNESS_INCIDENT_DATE="1585faa5-3145-4414-bebf-ff899fa7433f";
    public static final String WITNESS_PHONE_NUMBER="ee10fa87-4764-4dcb-8389-f34a05e1fb3d";//Was 2030cf99-54ca-4552-b4a5-a503eaad1342 - From the db
    public static final String WITNESS_INCIDENT_SEVERITY="f962fac9-1fae-413e-a16d-225161c07b22";
    public static final String WITNESS_NEWS_TYPE="a8168ad4-5c62-42a5-b1fa-97214157ea2f";
    public static final String WITNESS_MEDIA_TYPE="35ce67ff-52d8-4845-b99b-8b1c1a33ae6e";
    public static final String WITNESS_CULPRIT_TYPE="2546e2af-65e5-454b-a12c-e9407ba61ca7";
    public static final String WITNESS_CULPRIT_TITLE="e50bbc0b-fb90-4322-b141-9e79be88f022";
    public static final String WITNESS_CULPRIT_NAME="789a894f-9e0e-41ed-b4df-63b1558c0f72";
    public static final String WITNESS_NATURE_OF_VIOLATION="tags";

    public static final String API_VICTIM_LOCATION="33e655d8-829b-4a54-a09a-0105fb57f49d";
    public static final String VICTIM_GENDER="7169c823-4af2-4d36-9316-685178635b4c";
    public static final String VICTIM_NAME="5c72ef2c-26f2-400d-a216-bafe78459d1b";
    public static final String VICTIM_PHONE_NUMBER="4481ab58-c309-409d-8d05-5247fe92a462";//Was b09c6c38-7d9a-49f9-859e-4f7f427daaf5
    public static final String VICTIM_INCIDENT_DATE="28d9532a-c487-4a66-a4b7-3aebf5fdf9d4";
    public static final String VICTIM_INCIDENT_SEVERITY="14644c80-08a2-4002-92e7-457474bf5348";
    public static final String VICTIM_NEWS_TYPE="eb563ac6-7946-44f2-87f6-c4274aeb492b";
    public static final String VICTIM_MEDIA_TYPE="9d0e51e1-0019-4e78-80e0-339348a73076";
    public static final String VICTIM_CULPRIT_TYPE= "89ad3cdc-4688-448f-bf7e-5f6f0e2182af";
    public static final String VICTIM_CULPRIT_TITLE="c8c022b8-bf8e-4577-a076-86deb9c835c7";
    public static final String VICTIM_CULPRIT_NAME="34c36a24-875f-455b-9059-46df63bb5cda";
    public static final String VICTIM_NATURE_OF_VIOLATION="tags";

}
