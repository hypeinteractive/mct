package tz.or.mct.mct;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import java.util.Locale;
import tz.or.mct.mct.victim.VictimActivity;
import tz.or.mct.mct.witness.WitnessMainActivity;

public class MainActivity extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        changeLanguage();
        setContentView(R.layout.activity_main);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayUseLogoEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setTitle("");
    }

    public void onCardClicked(View view) {
        switch (view.getId()){
            case R.id.cv_victim:
               startActivity(new Intent(this, VictimActivity.class));
                break;
            case R.id.cv_witness:
                startActivity(new Intent(this, WitnessMainActivity.class));
                break;
            default:
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main,menu);
        SharedPreferences shp=getSharedPreferences("tz.or.mct.mct.preferences", 0);
        String languageLoaded = shp.getString("language","en");
        if (languageLoaded.equalsIgnoreCase("sw")){
            menu.findItem(R.id.action_change_language_sw).setIcon(R.drawable.ic_action_lang_sw_selected);
        }else {
            menu.findItem(R.id.action_change_language_en).setIcon(R.drawable.ic_action_lang_en_selected);
        }
        return super.onCreateOptionsMenu(menu);
    }


    @SuppressLint("RestrictedApi")
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();
        switch (id){
            case R.id.action_change_language_en:
                setCurrentLangauge("en");
                break;
            case R.id.action_change_language_sw:
                setCurrentLangauge("sw");
                break;
            default:
        }
        invalidateOptionsMenu();
        return super.onOptionsItemSelected(item);
    }

    private void changeLanguage(){
        SharedPreferences shp=getSharedPreferences("tz.or.mct.mct.preferences", 0);
        String languageToLoad = shp.getString("language","en");
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
    }

    private void setCurrentLangauge(String currentLangauge){
        SharedPreferences.Editor rEditor = getSharedPreferences("tz.or.mct.mct.preferences", 0).edit();
        rEditor.putString("language", currentLangauge);
        rEditor.apply();
        changeLanguage();
        recreate();
    }


}
