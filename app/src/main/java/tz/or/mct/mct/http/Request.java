package tz.or.mct.mct.http;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import tz.or.mct.mct.api.Config;
import tz.or.mct.mct.api.ParamKeys;
import tz.or.mct.mct.api.TokenRequest;

/**
 * Created by memory on 26/11/2016 for MCT Project.
 */

public class Request{
    public static final String METHOD_POST="POST";
    public static final String METHOD_GET="GET";
    private HashMap<String,String> params=new HashMap<>();
    private String method;
    private OnResponseReady orr;
    private Context context;
    private RequestType requestType;


    public Request(Context context,RequestType type) {
        this.context = context;
        this.requestType=type;
    }

    /**
     * @param context Application/Activity context
     * @param type one of the VICTIM_REPORT,WITNESS_REPORT,VICTIM_DETAILED_REPORT,WITNESS_DETAILED_REPORT,LOGIN,REGISTER
     * @param params HashMap with key value pair to be sent to the server
     */

    public Request(Context context,RequestType type,HashMap<String,String> params) {
        this.context = context;
        this.requestType=type;
        this.params=params;
    }

    public HashMap<String, String> getParams() {
        return params;
    }

    public Request setParams(HashMap<String, String> params) {
        this.params = params;
        return this;
    }

    public String getMethod() {
        return method;
    }

    public Request setMethod(String method) {
        this.method = method;
        return this;
    }

    public OnResponseReady getOnResponseReadyListener() {
        return orr;
    }

    public Request setOnResponseReadyListener(OnResponseReady orr) {
        this.orr = orr;
        return this;
    }

    public RequestType getRequestType() {
        return requestType;
    }

    public Request setRequestType(RequestType requestType) {
        this.requestType = requestType;
        return this;
    }

    public void send(){
        switch (getRequestType()){
            case VICTIM_REPORT:
                break;
            case WITNESS_REPORT:
                break;
            case LOGIN:
                break;
            case REGISTER:
                break;
            default:
                break;
        }
        String token=TokenRequest.getStoredToken(context);
        if (token!=null){
            HttpRequestAsyncTask hrat=new HttpRequestAsyncTask(context, Config.POSTS_REQUEST_URL,orr);
            try {
                hrat.setAuthHeader("Bearer "+token);
                hrat.execute(getPreparedDataForPosts());

            } catch (JSONException e) {
                e.printStackTrace();
                orr.onResponseFailed(e.toString());
            }
        }else{
            TokenRequest.getToken(context, new OnResponseReady() {
                @Override
                public void onResponseFailed(String msg) {

                }

                @Override
                public void onResponseOk(JSONObject jSONObject) {
                    try {
                        String token=jSONObject.getString("access_token");
                        TokenRequest.storeToken(context,token);
                        HttpRequestAsyncTask hrat=new HttpRequestAsyncTask(context, Config.POSTS_REQUEST_URL,orr);
                        hrat.setAuthHeader("Bearer "+token);
                        hrat.execute(getPreparedDataForPosts());
                    } catch (JSONException e) {
                        e.printStackTrace();
                        orr.onResponseFailed(e.toString());
                    }
                }
            });
        }
    }



    private JSONObject getPreparedDataForPosts() throws JSONException {
        JSONObject dataJObj=new JSONObject();
        dataJObj.put(ParamKeys.TITLE,params.get(ParamKeys.TITLE));
        dataJObj.put(ParamKeys.CONTENT,params.get(ParamKeys.CONTENT));
        params.remove(ParamKeys.TITLE);
        params.remove(ParamKeys.CONTENT);
        JSONObject formJObj=new JSONObject();

        int formId=(getRequestType().equals(RequestType.VICTIM_REPORT)?3:2);
        formJObj.put("id",formId);
        formJObj.put("url","http://45.55.206.169/api/v3/forms/"+formId); //2 for witness and 3 for victim
        formJObj.put("type","report");
        JSONObject valuesJObj = new JSONObject();
        if (params.containsKey(ParamKeys.LOCATION_LAT) && params.containsKey(ParamKeys.LOCATION_LONG)){
            JSONObject locJObj=new JSONObject();
            locJObj.put(ParamKeys.LOCATION_LAT,params.get(ParamKeys.LOCATION_LAT));
            locJObj.put(ParamKeys.LOCATION_LONG,params.get(ParamKeys.LOCATION_LONG));
            JSONArray locAr = new JSONArray();
            locAr.put(locJObj);
            valuesJObj.put(formId==2?ParamKeys.API_WITNESS_LOCATION:ParamKeys.API_VICTIM_LOCATION,locAr);
            params.remove(ParamKeys.LOCATION_LONG);
            params.remove(ParamKeys.LOCATION_LAT);
        }
        try{
            if (params.containsKey(ParamKeys.VICTIM_NATURE_OF_VIOLATION)){
                JSONArray violationNature = new JSONArray(params.get(ParamKeys.VICTIM_NATURE_OF_VIOLATION));
                dataJObj.put(ParamKeys.VICTIM_NATURE_OF_VIOLATION,violationNature);
                params.remove(ParamKeys.VICTIM_NATURE_OF_VIOLATION);
            }else if(params.containsKey(ParamKeys.WITNESS_NATURE_OF_VIOLATION)){
                JSONArray violationNature = new JSONArray(params.get(ParamKeys.WITNESS_NATURE_OF_VIOLATION));
                dataJObj.put(ParamKeys.WITNESS_NATURE_OF_VIOLATION,violationNature);
                params.remove(ParamKeys.WITNESS_NATURE_OF_VIOLATION);
            }
        }catch (JSONException e){
            throw new JSONException("Incident type must be an JSONArray converted to a string");
        }
        Object keys[]=params.keySet().toArray();
        for(Object key:keys){
            valuesJObj.put((String) key,new JSONArray().put(params.get(key)));
        }
        dataJObj.put("form",formJObj);
        dataJObj.put("values",valuesJObj);
        return dataJObj;
    }

    public enum RequestType{
        VICTIM_REPORT,WITNESS_REPORT,LOGIN,REGISTER
    }

    public interface OnResponseReady {
        void onResponseFailed(String msg);
        void onResponseOk(JSONObject jSONObject);
    }

    public static String testString="{\"title\":\"TEsting Victim Form\",\"content\":\"Testing\",\"locale\":\"en_US\",\"values\":\n" +
            "\t{\"28d9532a-c487-4a66-a4b7-3aebf5fdf9d4\":[\"2016-11-28T06:35:16.825Z\"]\n" +
            "\t\t,\"33e655d8-829b-4a54-a09a-0105fb57f49d\":[{\"lat\":-6.792354,\"lon\":39.20832840000003}],\"5c72ef2c-26f2-400d-a216-bafe78459d1b\":[\"Brian Paul\"],\n" +
            "\t\"b09c6c38-7d9a-49f9-859e-4f7f427daaf5\":[684788499]},\"completed_stages\":[],\"published_to\":[],\"form\":\n" +
            "\t{\"id\":3,\"url\":\"http://45.55.206.169/api/v3/forms/3\",\"parent_id\":null,\"name\":\"Victim Form\",\"description\":\"To be filled with a victim of an incidence\",\"color\":\"#E69327\",\"type\":\"report\",\"disabled\":false,\"created\":\"2016-10-04T09:39:29+00:00\",\"updated\":\"2016-11-23T14:48:35+00:00\",\"require_approval\":true,\"everyone_can_create\":true,\"can_create\":[],\"allowed_privileges\":[\"read\",\"create\",\"update\",\"delete\",\"search\"]},\"allowed_privileges\":[\"read\",\"search\"]}";
}
