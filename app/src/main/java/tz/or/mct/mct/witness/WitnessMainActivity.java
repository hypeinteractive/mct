package tz.or.mct.mct.witness;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.inputmethodservice.InputMethodService;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.seatgeek.placesautocomplete.OnPlaceSelectedListener;
import com.seatgeek.placesautocomplete.PlacesAutocompleteTextView;
import com.seatgeek.placesautocomplete.model.Place;

import org.json.JSONObject;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import tz.or.mct.mct.MainActivity;
import tz.or.mct.mct.R;
import tz.or.mct.mct.api.ParamKeys;
import tz.or.mct.mct.http.Request;

public class WitnessMainActivity extends AppCompatActivity implements View.OnClickListener, OnMapReadyCallback, tz.or.mct.mct.http.Request.OnResponseReady {
    private EditText title, incident, date, location, witnessPhone;
    private Button submitBasic, buttonSearch, buttonClear;
    private ImageView datePicker;
    private Calendar calendar;
    private DatePickerDialog chooseDate;
    private String witnessTitle;
    private String witnessIncident;
    private String dateEvent;
    private String witnessPhonuNumber;
    private MapFragment mapFragment;
    private GoogleMap myGoogleMap;
    private double latitude, longitude;
    private PreferenceTracking pref;
    private ProgressDialog pDialog;
    private String witnessLocation;
    private String locality;
    private Marker marker;
    final Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.witnessactivity_main);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initViews();
        if (googleServicesAvailable()) {
            initMap();
        } else {
            Toast.makeText(getApplicationContext(), R.string.playservices, Toast.LENGTH_SHORT).show();
        }

        pref = new PreferenceTracking(getApplicationContext());
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(R.string.activity_witness_main_title);
        actionBar.setLogo(R.mipmap.ic_logo);
        actionBar.setDisplayUseLogoEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
    }

    public void initViews() {
        title = (EditText) findViewById(R.id.witness_title);
        incident = (EditText) findViewById(R.id.witness_incident);
        date = (EditText) findViewById(R.id.et_date);
        location = (EditText) findViewById(R.id.location);
        witnessPhone = (EditText) findViewById(R.id.witness_phonenumber);
        datePicker = (ImageView) findViewById(R.id.witness_datepicker);
        submitBasic = (Button) findViewById(R.id.witness_submit);
        buttonSearch = (Button) findViewById(R.id.witness_search);
        buttonClear = (Button) findViewById(R.id.witness_clear);
        buttonSearch.setOnClickListener(this);
        buttonClear.setOnClickListener(this);
        datePicker.setOnClickListener(this);
        date.setOnClickListener(this);
        submitBasic.setOnClickListener(this);
        ((PlacesAutocompleteTextView)location).setOnPlaceSelectedListener(new OnPlaceSelectedListener() {
            @Override
            public void onPlaceSelected(@NonNull Place place) {
                onSearch(location);
            }
        });
        ((PlacesAutocompleteTextView)location).setLocationBiasEnabled(true);
        Location biasLocation = new Location("none");
        biasLocation.setLatitude(-6.178646);
        biasLocation.setLongitude(35.480995);
        ((PlacesAutocompleteTextView)location).setCurrentLocation(biasLocation);
        ((PlacesAutocompleteTextView)location).setRadiusMeters(632060L);
    }

    public void showDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(WitnessMainActivity.this);
        builder.setMessage(R.string.more_info);
        builder.setNegativeButton(R.string.witness_skip, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                submiBasicFullRecord();
                dialog.dismiss();
            }
        });
        builder.setPositiveButton(R.string.witness_provide, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(WitnessMainActivity.this, WitnessDetails.class);
                witnessPreference();
                startActivityForResult(intent,1);
                dialogInterface.dismiss();
            }
        });

        AlertDialog diag = builder.create();
        diag.show();
    }


    @Override
    public void onClick(View view) {
        if (view == datePicker || view==date) {
            getCalender();
            date.setError(null);
        } else if (view == submitBasic) {
            if (title.getText().length() == 0) {
                title.setError(getString(R.string.et_title));
                title.requestFocus();
            }else if (incident.getText().length() == 0) {
                incident.setError(getString(R.string.et_incident));
                incident.requestFocus();
            } else if (date.getText().length() <= 6) {
                date.setError(getString(R.string.dateRequired));
                date.requestFocus();
            } else if (location.getText().length() == 0) {
                location.setError(getString(R.string.location));
                location.requestFocus();
            }else  if ((witnessPhone.getText().toString().length() <= 9)) {
                witnessPhone.setError(getString(R.string.phone));
                witnessPhone.requestFocus();
            } else {
                //showDialog();
                showCustomDialog();
            }

        } else if (view == buttonSearch) {
            hideSystemKeybora(buttonSearch);
            onSearch(view);

        } else if (view == buttonClear) {
            location.setText("");
        }

    }


    public void getText1() {
        witnessTitle = title.getText().toString().trim();
        witnessIncident = incident.getText().toString().trim();
        dateEvent = date.getText().toString().trim();
        witnessPhonuNumber = witnessPhone.getText().toString().trim();
        witnessLocation = location.getText().toString().trim();
    }


    private void submiBasicFullRecord() {
        pDialog = new ProgressDialog(WitnessMainActivity.this);
        pDialog.setTitle(R.string.loading);
        pDialog.setMessage(getString(R.string.please));
        pDialog.setIndeterminate(true);
        pDialog.setCancelable(false);
        pDialog.show();
        HashMap<String, String> params = getFullBasicParams();
        new tz.or.mct.mct.http.Request(this, tz.or.mct.mct.http.Request.RequestType.WITNESS_REPORT, params)
                .setOnResponseReadyListener(this)
                .setMethod(Request.METHOD_POST)
                .send();
    }

    public HashMap<String, String> getFullBasicParams() {
        getText1();
        HashMap<String, String> params = new HashMap<>();

        params.put(ParamKeys.TITLE, witnessTitle);
        params.put(ParamKeys.CONTENT, witnessIncident);
        pref.setWitnessTitle(witnessTitle);
        pref.setWitnessIncident(witnessIncident);
        if (!dateEvent.equals("")) {
            params.put(ParamKeys.WITNESS_INCIDENT_DATE, dateEvent);
            pref.setWitnessIncidentDate(dateEvent);
        }
        if (!witnessPhonuNumber.equals("")) {
            params.put(ParamKeys.WITNESS_PHONE_NUMBER, witnessPhonuNumber);
            pref.setMobileNumber(witnessPhonuNumber);
        }
        if (!String.valueOf(latitude).equals("")) {
            params.put(ParamKeys.LOCATION_LAT, String.valueOf(latitude));
            pref.setWitnessLatitude(String.valueOf(latitude));
        }
        if (!String.valueOf(longitude).equals("")) {
            params.put(ParamKeys.LOCATION_LONG, String.valueOf(longitude));
            pref.setWitnessLongitude(String.valueOf(longitude));
        }
        return params;
    }

    public void getCalender() {
        Date today = new Date();
        calendar = Calendar.getInstance();
        calendar.setTime(today);
        chooseDate = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                String yearEvent = String.valueOf(year);
                String monthEvent = String.valueOf(month + 1);
                String dayEvent = String.valueOf(day);
                date.setText(dayEvent+ "-" + monthEvent + "-" + yearEvent );
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        chooseDate.show();
    }

    public boolean googleServicesAvailable() {
        GoogleApiAvailability api = GoogleApiAvailability.getInstance();
        int isAvailable = api.isGooglePlayServicesAvailable(this);
        if (isAvailable == ConnectionResult.SUCCESS) {
            return true;
        } else if (api.isUserResolvableError(isAvailable)) {
            Dialog dialog = api.getErrorDialog(this, isAvailable, 0);
            dialog.show();
        } else {
            Toast.makeText(getApplicationContext(), R.string.cantConnect, Toast.LENGTH_SHORT).show();
        }
        return false;
    }

    private void initMap() {
        mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        myGoogleMap = googleMap;
        LatLng tanzania = new LatLng(-6.3690, 34.8888);
        marker=myGoogleMap.addMarker(new MarkerOptions().position(tanzania).title(""));
        myGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(tanzania, 6));

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling ActivityCompat#requestPermissions
            return;
        }
        myGoogleMap.setMyLocationEnabled(true);
    }

    public void onSearch(View v) {
        String locationEvent2 = location.getText().toString().trim();
        List<android.location.Address> adressList = null;
        if (location != null || !location.equals("")) {
            Geocoder geocoder = new Geocoder(this);
            try {
                adressList = geocoder.getFromLocationName(locationEvent2, 1);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if ((adressList != null ? adressList.size() : 0) > 0) {
                android.location.Address address = adressList.get(0);
                locality = address.getLocality();
                LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());
                if (marker != null) {
                    marker.remove();
                }
                marker = myGoogleMap.addMarker(new MarkerOptions()
                        .position(latLng)
                        .title(locality));
                myGoogleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
                latitude = address.getLatitude();
                longitude = address.getLongitude();
            }

        }
    }
    @Override
    public void onResponseFailed(String msg) {
        pDialog.dismiss();
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onResponseOk(JSONObject jSONObject) {
        pDialog.dismiss();
        showDialogResponse();

    }

    void hideSystemKeybora(View view) {
        InputMethodManager inputs = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        inputs.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void witnessPreference(){
        SharedPreferences witnessPreference = getSharedPreferences("witnessPreference", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = witnessPreference.edit();
        editor.putString("witnessPhone",witnessPhonuNumber);
        editor.putString("witnessTittle",witnessTitle);
        editor.putString("witnessIncidence",witnessIncident);
        editor.putString("witnessDate", dateEvent);
        editor.putString("witnessLocation",witnessLocation);
        editor.commit();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if(resultCode == RESULT_OK){
                String Phone = data.getStringExtra("phone");
                String Tittle = data.getStringExtra("title");
                String Incidence = data.getStringExtra("incidence");
                String Date = data.getStringExtra("date");
                String Location = data.getStringExtra("location");

                witnessPhone.setText(Phone);
                location.setText(Location);
                date.setText(Date);
                title.setText(Tittle);
                incident.setText(Incidence);
            }
        }
    }
    public void showDialogResponse() {
        AlertDialog.Builder builder = new AlertDialog.Builder(WitnessMainActivity.this);
        builder.setMessage(R.string.submitted);
        builder.setPositiveButton(R.string.OK, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(WitnessMainActivity.this, MainActivity.class);
                startActivity(intent);
                dialogInterface.dismiss();
            }
        });
        AlertDialog diag = builder.create();
        diag.show();
    }

    private void showCustomDialog(){
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.custom_dialog);
        dialog.setTitle(R.string.more_info);
        TextView text = (TextView) dialog.findViewById(R.id.tv_header);
        text.setText(R.string.submitted_message);
        Button bn_ok = (Button) dialog.findViewById(R.id.bn_ok);
        bn_ok.setText(R.string.OK);
        bn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(WitnessMainActivity.this, WitnessDetails.class);
                witnessPreference();
                startActivityForResult(intent,1);
                dialog.dismiss();
            }
        });
        Button bn_cancel = (Button) dialog.findViewById(R.id.bn_thank);
        bn_cancel.setText(R.string.witness_skip);
        bn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submiBasicFullRecord();
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}