package tz.or.mct.mct.witness;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by hezroni on 11/28/2016.
 */
public class PreferenceTracking {
    private final SharedPreferences.Editor editor;
    private SharedPreferences preferences;
    Context context;
    private String PREF_NAME = "WITNESS CHOICE";
    private String ALL_BAIC_FILLED = "ALL_FILLED";
    private String MOBILE_NUMBER = "MOBILE_NUMBER";
    private String WITNESS_TITLE = "WITNESS_TITLE";
    private String WITNESS_INCIDENT = "WITNESS_INCIDENT";
    private String WITNESS_SEX = "WITNESS_SEX";
    private String WITNESS_NAME = "WITNESS_NAME";
    private String WITNESS_VICTIM_NAME = "WITNESS_VICTIM_NAME";
    private String WITNESS_LATITUDE = "WITNESS_LATITUDE";
    private String WITNESS_LONGITUDE = "WITNESS_LONGITUDE";
    private String WITNESS_INCIDENT_DATE = "WITNESS_INCIDATE_DATE";

    PreferenceTracking(Context context) {
        this.context = context;
        preferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = preferences.edit();
    }

    public boolean isBasicFull() {
        return preferences.getBoolean(ALL_BAIC_FILLED, false);
    }

    public void setBasicFull(boolean full) {
        editor.putBoolean(ALL_BAIC_FILLED, full);
        editor.commit();
    }

    public void setMobileNumber(String mobileNumber) {
        editor.putString(MOBILE_NUMBER, mobileNumber);
        editor.commit();
    }

    public String getMobileNumber() {

        return preferences.getString(MOBILE_NUMBER, "");
    }

    public void setWitnessTitle(String witnessTitle) {
        editor.putString(WITNESS_TITLE, witnessTitle);
        editor.commit();
    }

    public String getWitnessTitle() {

        return preferences.getString(WITNESS_TITLE, "");
    }

    public void setWitnessIncident(String witnessIncident) {
        editor.putString(WITNESS_INCIDENT, witnessIncident);
        editor.commit();
    }

    public String getWitnessIncident() {

        return preferences.getString(WITNESS_INCIDENT, "");
    }

    public void setWitnessSex(String witnessSex) {
        editor.putString(WITNESS_SEX, witnessSex);
        editor.commit();
    }

    public String getWitnessSex() {

        return preferences.getString(WITNESS_SEX, "");
    }

    public void setWitnessName(String witnessName) {
        editor.putString(WITNESS_NAME, witnessName);
        editor.commit();
    }

    public String getWitnessName() {
        return preferences.getString(WITNESS_NAME, "");
    }

    public void setWitnessVictimName(String witnessVictinName) {
        editor.putString(WITNESS_VICTIM_NAME, witnessVictinName);
        editor.commit();
    }

    public String getWitVictimName() {

        return preferences.getString(WITNESS_VICTIM_NAME, "");
    }

    public void setWitnessLatitude(String witnessLatitude) {
        editor.putString(WITNESS_LATITUDE, witnessLatitude);
        editor.commit();
    }

    public String getWitLatitude() {

        return preferences.getString(WITNESS_LATITUDE, " ");
    }

    public void setWitnessLongitude(String witnessLongitude) {
        editor.putString(WITNESS_LONGITUDE, witnessLongitude);
        editor.commit();
    }

    public String getWitLongitude() {

        return preferences.getString(WITNESS_LONGITUDE, "");
    }
    public void setWitnessIncidentDate(String witnessIncidentDate) {
        editor.putString(WITNESS_INCIDENT_DATE, witnessIncidentDate);
        editor.commit();
    }

    public String getWitnessIncidentDate() {

        return preferences.getString(WITNESS_INCIDENT_DATE, "");
    }




}
