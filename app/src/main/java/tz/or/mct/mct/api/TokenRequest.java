package tz.or.mct.mct.api;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import tz.or.mct.mct.http.HttpRequestAsyncTask;
import tz.or.mct.mct.http.Request;

/**
 * Created by memory on 27/11/2016.
 */

public class TokenRequest {
    private static final String KEY_TOKEN="tz.or.mct.mct.access_token";
    private static final String KEY_EXPIRES="tz.or.mct.mct.expires";

    public static void getToken(Context context, Request.OnResponseReady orr){
        JSONObject object=new JSONObject();
        try{
            object.put("client_id",Config.API_CLIENT_ID);
            object.put("client_secret",Config.API_CLIENT_SECRET);
            object.put("grant_type",Config.API_CLIENT_GRANT_TYPE);
            object.put("scope",Config.API_CLIENT_SCOPE);
            HttpRequestAsyncTask hrat=new HttpRequestAsyncTask(context, Config.TOKEN_REQUEST_URL,orr);
            hrat.execute(object);
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    public static String getStoredToken(Context con){
        SharedPreferences shp = con.getSharedPreferences("tz.or.mct.mct.preferences", 0);
        return checkTokenValidity(con)?shp.getString(TokenRequest.KEY_TOKEN,null):null;
    }

    public static void storeToken(Context con,String token){
        SharedPreferences.Editor shpe = con.getSharedPreferences("tz.or.mct.mct.preferences", 0).edit();
        shpe.putString(KEY_TOKEN, token);
        shpe.putLong(KEY_EXPIRES, (System.currentTimeMillis()-30000));
        shpe.apply();
    }

    private static boolean checkTokenValidity(Context con){
        SharedPreferences shp = con.getSharedPreferences("tz.or.mct.mct.preferences", 0);
        String token=shp.getString(TokenRequest.KEY_TOKEN,null);
        double expires=shp.getLong(TokenRequest.KEY_EXPIRES,0);
        if (token==null){
            return false;
        }else if (expires<System.currentTimeMillis()){
            shp.edit().clear().apply();
            return false;
        }
        return true;
    }
}
