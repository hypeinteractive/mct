package tz.or.mct.mct.victim;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import tz.or.mct.mct.MainActivity;
import tz.or.mct.mct.R;
import tz.or.mct.mct.api.ParamKeys;
import tz.or.mct.mct.http.Request;

public class VictimMoreDetailsActivity extends AppCompatActivity implements View.OnClickListener,Request.OnResponseReady {
    Button bnSearch, bnClear, bnSubmit;
    CheckBox cbDenied, cbThreat, cbMurder, cbArrest, cbKidnap, cbAssault, cbBattery, cbBanning, cbViolent, cbConfiscation, cbCyber,cbHarassment, cbDamage;
    EditText etCulpritName, etCulpritTitle,  etVictimName;
    RadioButton radioSex, radioCulprit, radioTypeOfNews, radioTypeOfMedia, radioIncidentSeverity;
    RadioGroup rgGender, rgCulpritType, rgTypeOfNews, rgTypeOfMedia, rgIncidentSeverity;
    List<String> collectedInformation = new ArrayList<>();
    private String sexType, Culprit, NewsType, TypeOfMedia, IncidentSeverity,victimName;;
    private String victimCulpritTitle;
    private String victimCulpritName;
    private JSONArray checkedData;
    private ProgressDialog pDialog;
    private HashMap<String,String> basicParams;
    private String sexSend;
    final Context context = this;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_victim_more_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.tv_heading);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        basicParams = (HashMap<String, String>) getIntent().getSerializableExtra("basic_params");
        initViews();
        ActionBar actionBar = getSupportActionBar();
        actionBar.setLogo(R.mipmap.ic_logo);
        actionBar.setDisplayUseLogoEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        setCheckListener();
    }

    @Nullable
    public Intent getParentActivityIntent() {
        return super.getParentActivityIntent();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() != android.R.id.home) {
            return super.onOptionsItemSelected(item);
        }
        finish();
        return true;
    }

    public Intent getSupportParentActivityIntent() {
        finish();
        return new Intent();
    }

    public void onCreateSupportNavigateUpTaskStack(TaskStackBuilder builder) {
        super.onCreateSupportNavigateUpTaskStack(builder);
    }

    public void initViews() {
        etCulpritName = (EditText) findViewById(R.id.etCulpritName);
        etCulpritTitle = (EditText) findViewById(R.id.etCulpritTitle);
        etVictimName = (EditText) findViewById(R.id.etName);
        bnSearch = (Button) findViewById(R.id.bnSearch);
        bnClear = (Button) findViewById(R.id.bnClear);
        cbDenied = (CheckBox) findViewById(R.id.cbDenied);
        cbThreat = (CheckBox) findViewById(R.id.cbThreat);
        cbMurder = (CheckBox) findViewById(R.id.cbMurder);
        cbArrest = (CheckBox) findViewById(R.id.cbArrest);
        cbKidnap = (CheckBox) findViewById(R.id.cbKidnap);
        cbAssault = (CheckBox) findViewById(R.id.cbAssault);
        cbBattery = (CheckBox) findViewById(R.id.cbBattery);
        cbBanning = (CheckBox) findViewById(R.id.cbBanning);
        cbViolent = (CheckBox) findViewById(R.id.cbViolent);
        cbDamage = (CheckBox) findViewById(R.id.cbDamage);
        cbCyber = (CheckBox) findViewById(R.id.cbCyber);
        cbHarassment = (CheckBox) findViewById(R.id.cbHarassment);
        rgGender = (RadioGroup) findViewById(R.id.rgGender);
        rgCulpritType = (RadioGroup) findViewById(R.id.rgCulpritType);
        rgTypeOfNews = (RadioGroup) findViewById(R.id.rgTypeOfNews);
        rgTypeOfMedia = (RadioGroup) findViewById(R.id.rgTypeOfMedia);
        rgIncidentSeverity = (RadioGroup) findViewById(R.id.rgIncidentSeverity);
        cbConfiscation = (CheckBox) findViewById(R.id.cbConfiscation);
        bnSubmit = (Button) findViewById(R.id.bnSubmit);
        bnSubmit.setOnClickListener(this);
    }

    public void sendVictimInformation() {
        sendCheckedItems();
        HashMap params = getVictimInformation();
        params.putAll(basicParams);
        new Request(this, Request.RequestType.VICTIM_REPORT, params)
                .setOnResponseReadyListener(this)
                .setMethod(Request.METHOD_POST)
                .send();
        pDialog = new ProgressDialog(this);
        pDialog.setTitle(R.string.loading);
        pDialog.setMessage(getString(R.string.please));
        pDialog.setIndeterminate(true);
        pDialog.setCancelable(false);
        pDialog.show();
    }

    public HashMap<String, String> getVictimInformation() {
        getInfo();
        HashMap<String, String> params = new HashMap<>();
        if (!sexType.isEmpty()) {
            params.put(ParamKeys.VICTIM_GENDER, sexType);
        }
        if (!victimName.isEmpty()) {
            params.put(ParamKeys.VICTIM_NAME, victimName);
        }
        if (!Culprit.isEmpty()) {
            params.put(ParamKeys.VICTIM_CULPRIT_TYPE, Culprit);
        }

        if (!NewsType.isEmpty()) {
            params.put(ParamKeys.VICTIM_NEWS_TYPE, NewsType);
        }

        if (!TypeOfMedia.isEmpty()) {
            params.put(ParamKeys.VICTIM_MEDIA_TYPE, TypeOfMedia);
        }

        if (!IncidentSeverity.isEmpty()) {
            params.put(ParamKeys.VICTIM_INCIDENT_SEVERITY, IncidentSeverity);
        }

        if (!victimCulpritName.equals("")) {
            params.put(ParamKeys.VICTIM_CULPRIT_NAME, victimCulpritName);
        }
        if (!victimCulpritTitle.equals("")) {
            params.put(ParamKeys.VICTIM_CULPRIT_TITLE, victimCulpritTitle);
        }
        if (checkedData!=null && !checkedData.toString().equalsIgnoreCase("")) {
            params.put(ParamKeys.VICTIM_NATURE_OF_VIOLATION, checkedData.toString());
        }
        return params;
    }


    public void getInfo() {
        victimCulpritTitle = etCulpritTitle.getText().toString();
        victimCulpritName = etCulpritName.getText().toString();
        victimName = etVictimName.getText().toString();

        //CHECKING FOR SEX VALUE
        int selectedGenderId = rgGender.getCheckedRadioButtonId();
        radioSex = (RadioButton) findViewById(selectedGenderId);
        sexSend = radioSex==null?"":radioSex.getText().toString();
        if (sexSend.equalsIgnoreCase("MME") || sexSend.equalsIgnoreCase("Male")){
            sexSend = "Male";
        }else if (sexSend.equalsIgnoreCase("MKE") || sexSend.equalsIgnoreCase("Female")){
            sexSend = "Female";
        }
        sexType = radioSex != null ? radioSex.getText().toString() : "";

        //CHECKING FOR CULPRIT
        int selectedCulpritId = rgCulpritType.getCheckedRadioButtonId();
        radioCulprit = (RadioButton) findViewById(selectedCulpritId);
        String sendCulprit = radioCulprit==null?"":radioCulprit.getText().toString();
        if (sendCulprit.equalsIgnoreCase("State Coercive Organs (intelligence and security agents, police, local government, militia)") || sendCulprit.equalsIgnoreCase("Mamlaka za nchi (mawakala wa usalama na upelelezi, polisi, serikali ya mtaa, jeshi)")){
            sendCulprit = "State Coercive Organs (intelligence and security agents, police, local government, militia)";
        } else if(sendCulprit.equalsIgnoreCase("Media owners/Shareholders/Directors") || sendCulprit.equalsIgnoreCase("Wamiliki/Waongozaji/Washiriki wa vyombo ya habari")){
            sendCulprit = "Media owners/Shareholders/Directors";
        } else if (sendCulprit.equalsIgnoreCase("Advertisers, Business and Political Allies of Owners") || sendCulprit.equalsIgnoreCase("Watangazaji, Biashara na Washirika wa kisiasa wa wamiliki")){
            sendCulprit = "Advertisers, Business and Political Allies of Owners";
        } else if(sendCulprit.equalsIgnoreCase("Politicians/State Functionaries") || sendCulprit.equalsIgnoreCase("Wanasiasa/Watendaji wa nchi")){
            sendCulprit = "Politicians/State Functionaries";
        }
        else if(sendCulprit.equalsIgnoreCase("Mob Assault") || sendCulprit.equalsIgnoreCase("Shambulizi la watu wengi")){
            sendCulprit = "Mob Assault";
        }
        Culprit = radioCulprit != null ? sendCulprit : "";

        //CHECKING FOR  TYPE OF NEWS
        int selectedNewsTypeId = rgTypeOfNews.getCheckedRadioButtonId();
        radioTypeOfNews = (RadioButton) findViewById(selectedNewsTypeId);
        String TypeOfNewsSend = radioTypeOfNews==null?"":radioTypeOfNews.getText().toString();
        if (TypeOfNewsSend.equalsIgnoreCase("Politics") || TypeOfNewsSend.equalsIgnoreCase("Siasa")){
            TypeOfNewsSend = "Politics";
        } else if(TypeOfNewsSend.equalsIgnoreCase("Social") || TypeOfNewsSend.equalsIgnoreCase("Jamii")){
            TypeOfNewsSend = "Social";
        } else if (TypeOfNewsSend.equalsIgnoreCase("Economics/Business") || TypeOfNewsSend.equalsIgnoreCase("Uchumi/Biashara")){
            TypeOfNewsSend = "Economics/Business";
        } else if (TypeOfNewsSend.equalsIgnoreCase("Environment") || TypeOfNewsSend.equalsIgnoreCase("Mazingira")){
            TypeOfNewsSend = "Environment";
        } else if (TypeOfNewsSend.equalsIgnoreCase("Government") || TypeOfNewsSend.equalsIgnoreCase("Serikali")){
            TypeOfNewsSend = "Government";
        } else if (TypeOfNewsSend.equalsIgnoreCase("Sports") || TypeOfNewsSend.equalsIgnoreCase("Michezo")){
            TypeOfNewsSend = "Sports";
        }
        NewsType = radioTypeOfNews != null ? TypeOfNewsSend : "";
        //CHECKING FOR TYPE OF MEDIA
        int selectedMediaTypeId = rgTypeOfMedia.getCheckedRadioButtonId();
        radioTypeOfMedia = (RadioButton) findViewById(selectedMediaTypeId);
        String TypeOfMediaSend = radioTypeOfMedia==null?"":radioTypeOfMedia.getText().toString();
        if (TypeOfNewsSend.equalsIgnoreCase("Print") || TypeOfMediaSend.equalsIgnoreCase("Chapisho")){
            TypeOfMediaSend = "Print";
        } else if (TypeOfMediaSend.equalsIgnoreCase("Radio") || TypeOfMediaSend.equalsIgnoreCase("Redio")){
            TypeOfMediaSend = "Radio";
        } else if (TypeOfMediaSend.equalsIgnoreCase("TV") || TypeOfMediaSend.equalsIgnoreCase("TV")){
            TypeOfMediaSend = "TV";
        } else if (TypeOfMediaSend.equalsIgnoreCase("Online Media") || TypeOfMediaSend.equalsIgnoreCase("Chombo cha mtandaoni")){
            TypeOfMediaSend = "Online Media";
        }
        TypeOfMedia = radioTypeOfMedia != null ? TypeOfMediaSend : "";
        int selectedIncidentSeverityId = rgIncidentSeverity.getCheckedRadioButtonId();
        radioIncidentSeverity = (RadioButton) findViewById(selectedIncidentSeverityId);
        String severitySend = radioIncidentSeverity==null?"":radioIncidentSeverity.getText().toString();
        if (severitySend.equalsIgnoreCase("Critical") || severitySend.equalsIgnoreCase("Kubwa sana")){
            severitySend = "Critical";
        } else if (severitySend.equalsIgnoreCase("High") || severitySend.equalsIgnoreCase("Kubwa")){
            severitySend = "High";
        } else if (severitySend.equalsIgnoreCase("Medium") || severitySend.equalsIgnoreCase("Kati kati")){
            severitySend = "Medium";
        } else if (severitySend.equalsIgnoreCase("Low") || severitySend.equalsIgnoreCase("Dogo")){
            severitySend = "Low";
        }
        IncidentSeverity = radioIncidentSeverity != null ? severitySend : "";
    }



    @Override
    public void onClick(View v) {
       if (v == bnSubmit) {
           sendVictimInformation();
        } else if (v == cbDenied) {
            if (cbDenied.isChecked()) {
                collectedInformation.add("1");
            } else {
                collectedInformation.remove("1");
            }
        } else if (v == cbThreat) {
            if (cbThreat.isChecked()) {
                collectedInformation.add("2");
            } else {
                collectedInformation.remove("2");
            }
        } else if (v == cbMurder) {
            if (cbMurder.isChecked()) {
                collectedInformation.add("3");
            } else {
                collectedInformation.remove("3");
            }
        } else if (v == cbHarassment) {
           if (cbHarassment.isChecked()) {
               collectedInformation.add("4");
           } else {
               collectedInformation.remove("4");
           }
           }else if (v == cbArrest) {
            if (cbArrest.isChecked()) {
                collectedInformation.add("5");
            } else {
                collectedInformation.remove("5");
            }
        } else if (v == cbKidnap) {
            if (cbKidnap.isChecked()) {
                collectedInformation.add("6");
            } else {
                collectedInformation.remove("6");
            }
        } else if (v == cbAssault) {
            if (cbAssault.isChecked()) {
                collectedInformation.add("7");
            } else {
                collectedInformation.remove("7");
            }
        } else if (v == cbBattery) {
            if (cbBattery.isChecked()) {
                collectedInformation.add("8");
            } else {
                collectedInformation.remove("8");
            }
        } else if (v == cbBanning) {
            if (cbBanning.isChecked()) {
                collectedInformation.add("9");
            } else {
                collectedInformation.remove("9");
            }
        } else if (v == cbViolent) {
            if (cbViolent.isChecked()) {
                collectedInformation.add("10");
            } else {
                collectedInformation.remove("10");
            }
        } else if (v == cbConfiscation) {
            if (cbConfiscation.isChecked()) {
                collectedInformation.add("11");
            } else {
                collectedInformation.remove("11");
            }
        } else if (v == cbDamage) {
            if (cbDamage.isChecked()) {
                collectedInformation.add("12");
            } else {
                collectedInformation.remove("12");
            }
        } else if (v == cbCyber) {
            if (cbCyber.isChecked()) {
                collectedInformation.add("13");
            } else {
                collectedInformation.remove("13");
            }
        }

    }


    public void setCheckListener() {
        cbDenied.setOnClickListener(this);
        cbThreat.setOnClickListener(this);
        cbMurder.setOnClickListener(this);
        cbArrest.setOnClickListener(this);
        cbKidnap.setOnClickListener(this);
        cbAssault.setOnClickListener(this);
        cbHarassment.setOnClickListener(this);
        cbBattery.setOnClickListener(this);
        cbBanning.setOnClickListener(this);
        cbViolent.setOnClickListener(this);
        cbConfiscation.setOnClickListener(this);
        cbCyber.setOnClickListener(this);
        cbDamage.setOnClickListener(this);

    }

    public void sendCheckedItems() {
        checkedData = new JSONArray();
        for (int i = 0; i < collectedInformation.size(); i++) {
            checkedData.put(collectedInformation.get(i));
        }
    }

    @Override
    public void onResponseFailed(String msg) {
        pDialog.hide();
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onResponseOk(JSONObject jSONObject) {
        pDialog.hide();
        //showDialogResponse();
        showFinalDialog();
    }
    public void showDialogResponse() {
        AlertDialog.Builder builder = new AlertDialog.Builder(VictimMoreDetailsActivity.this);
        builder.setMessage(R.string.submitted);
        builder.setPositiveButton(R.string.OK, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(VictimMoreDetailsActivity.this, MainActivity.class);
                startActivity(intent);
                dialogInterface.dismiss();
            }
        });
        AlertDialog diag = builder.create();
        diag.show();
    }

    private void showFinalDialog(){
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.custom_dialog_final);
        dialog.setTitle(R.string.submitted);
        TextView text = (TextView) dialog.findViewById(R.id.tv_header);
        text.setText(R.string.submitted_final);
        Button bn_ok = (Button) dialog.findViewById(R.id.bn_final);
        bn_ok.setText(R.string.OK);
        bn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(VictimMoreDetailsActivity.this, MainActivity.class);
                startActivity(intent);
                dialog.dismiss();
            }
        });
        dialog.show();
    }

}
