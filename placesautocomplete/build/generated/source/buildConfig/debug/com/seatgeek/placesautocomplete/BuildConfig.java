/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.seatgeek.placesautocomplete;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String LIBRARY_PACKAGE_NAME = "com.seatgeek.placesautocomplete";
  public static final String BUILD_TYPE = "debug";
  public static final int VERSION_CODE = 2;
  public static final String VERSION_NAME = "0.2-SNAPSHOT";
}
